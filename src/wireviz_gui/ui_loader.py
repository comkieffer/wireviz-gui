import logging
import sys
from os import R_OK, access
from os.path import isfile

from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import QWidget



def load_ui(ui_file_name: str, parent: QWidget = None) -> QWidget:
    """Load a .ui file and construct a QWidget class out of it.

    If the QUiLoder failed to parse the file, an error is logged and the program
    terminates. The expectation is that not being to create a window is an unrecoverable
    error.

    Arguments:
        ui_file_name {str} -- Path to the .ui file

    Returns:
        WidgetType -- QWidget-derived class for the specified window file.
    """
    logging.debug(f"Loading ui file from {ui_file_name}, parent={parent} ...")

    if not isfile(ui_file_name):
        logging.critical(f"Ui file '{ui_file_name}' does not exist. Cannot proceed.")
        sys.exit(1)

    if not access(ui_file_name, R_OK):
        logging.critical(f"No read access to '{ui_file_name}'. Cannot proceed.")
        sys.exit(1)

    loader = QUiLoader()
    window = loader.load(ui_file_name, parent)

    if not window:
        logging.critical(f"Failed to create window from {ui_file_name}. Cannot proceed.")
        logging.critical(f"Error was: {loader.errorString()}")
        sys.exit(1)

    return window
