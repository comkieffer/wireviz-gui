
import sys

import click

from PySide6.QtWidgets import QApplication

from .logging import initialise_logging
from .main_window import WireVizMainWindow

@click.command()
@click.option("-v", "--verbose", is_flag=True, default=False)
@click.argument("input-file", type=click.Path(exists=True), required=False)
def main(verbose, input_file):
    initialise_logging(verbose)

    app = QApplication(sys.argv)

    main_window = WireVizMainWindow(app)
    if input_file:
        main_window.load_file(input_file)

    main_window.ui.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
