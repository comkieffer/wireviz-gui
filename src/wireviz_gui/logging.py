import logging

from typing import Union

from . import APP_NAME


def initialise_logging(verbose: bool) -> None:
    logging.basicConfig(
        format='%(asctime)s: [%(levelname)7s] - %(name)s %(message)s',
        level=logging.DEBUG if verbose else logging.INFO,
    )


class QTextEditHandler(logging.Handler):

    def __init__(self, widget, level: Union[int, str] = logging.NOTSET):
        super().__init__(level=level)
        self._widget = widget

    def emit(self, record):
        self._widget.append(self.format(record))

        if record.exc_info:  # for exceptions
            self.widget.append(
                logging._defaultFormatter.formatException(record.exc_info)
            )


def register_textedit_handler(widget, level: int = logging.INFO, format: str = "[%(levelname)7s] %(message)s"):
    textedit_handler = QTextEditHandler(widget, level)
    textedit_handler.setFormatter(logging.Formatter(format))

    logging.getLogger().addHandler(textedit_handler)
