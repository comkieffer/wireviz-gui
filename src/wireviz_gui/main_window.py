
from os import path
import logging

from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional
from PySide6.QtGui import QPixmap

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QFileDialog, QFrame, QLabel, QMessageBox

from wireviz.wireviz import parse
from wireviz.Harness import Harness

import wireviz_gui.ui.ui_resources

from .import __version__
from .ui_loader import load_ui
from .logging import register_textedit_handler


class WireVizMainWindow:

    def __init__(self, parent=None):
        package_dir = path.dirname(path.abspath(__file__))
        self.ui = load_ui(path.join(package_dir, "ui", "main_window.ui"))

        register_textedit_handler(self.ui.log_pane)

        # Class state variables
        self.wireviz_yaml: Optional[str] = None
        self.wireviz_input_file: Optional[Path] = None
        self.wireviz_harness: Optional[Harness] = None

        self._generate_svg = self.ui.generate_svg_cb.isChecked()
        self._generate_png = self.ui.generate_png_cb.isChecked()
        self._generate_html = self.ui.generate_html_cb.isChecked()

        # Connect Signals and slots
        self.ui.action_open.triggered.connect(self._on_open_clicked)
        self.ui.action_about.triggered.connect(self._on_about_clicked)
        self.ui.save_btn.clicked.connect(self._on_save_clicked)

        # Set-up status bar
        self._current_file_sb = QLabel(self.wireviz_input_file or "")
        self.ui.statusbar.addWidget(self._current_file_sb)

        pipe = QFrame()
        pipe.setFrameShape(QFrame.HLine)
        pipe.setFrameShadow(QFrame.Sunken)
        self.ui.statusbar.addWidget(pipe)

        self._current_save_state_sb = QLabel("Unsaved")
        self.ui.statusbar.addPermanentWidget(self._current_save_state_sb)

    def run_wireviz(self):
        logging.info("Generating wireviz preview ... ")
        if not self.wireviz_input_file:
            logging.error("Cannot generate output file. Input file not set.")
            return

        assert self.wireviz_input_file.is_file()

        with self.wireviz_input_file.open("r") as input_file:
            yaml_content = input_file.read()

        self.wireviz_harness = parse(yaml_content, return_types="harness")

        with TemporaryDirectory() as tmp_dir:
            png_file_name = Path(tmp_dir) / "wireviz-generated.png"
            logging.debug(f"Saving png output to {png_file_name} ... ")
            with open(png_file_name, "wb+") as png_file:
                png_file.write(self.wireviz_harness.png)

            pixmap = QPixmap()
            pixmap.load(str(png_file_name))
            pixmap.scaled(
                self.ui.generated_image.size(),
                Qt.KeepAspectRatio,
                Qt.SmoothTransformation
            )

            self.ui.generated_image.setPixmap(pixmap)


    def generate_wireviz_outputs(self, output_dir: Path):
        assert output_dir.exists()

        harness_name = self.wireviz_input_file.stem
        if self._generate_png:
            png_file = output_dir / (harness_name + ".png")
            logging.info(f"Saving png file to {png_file} ...")
            png_file.write_bytes(self.wireviz_harness.png)

        if self._generate_svg or self._generate_html:
            svg_file = output_dir / (harness_name + ".svg")
            logging.info(f"Saving svg file to {svg_file} ...")
            svg_file.write_bytes(self.wireviz_harness.svg)

        if self._generate_html:
            html_file = output_dir / (harness_name)
            logging.info(f"Saving html file to {html_file} ...")
            self.wireviz_harness.output(html_file)

            if not self._generate_svg:
                logging.info(f"Removing svg file to {svg_file} ...")
                svg_file.unlink()

        self._current_save_state_sb.setText("State: Saved")

    def load_file(self, filename: Path):
        logging.debug(f"User selected file {self.wireviz_input_file}")

        self.wireviz_harness = None
        self.wireviz_input_file = Path(filename)

        self.wireviz_yaml = self.wireviz_input_file.read_text()
        logging.debug(f"Read {len(self.wireviz_yaml)} characters from harness description file.")

        self.ui.harness_desc.setPlainText(self.wireviz_yaml)

        self._current_file_sb.setText(f"Current file: {self.wireviz_input_file}")
        self._current_save_state_sb.setText("State: Unsaved")

        self.run_wireviz()


    def _on_open_clicked(self):
        file_picker = QFileDialog(self.ui, caption="Pick Input File")
        file_picker.setFileMode(QFileDialog.FileMode.ExistingFiles)
        file_picker.setNameFilter("WireViz Files (*.yml *.yaml);; All Files (*)")

        if file_picker.exec_():
            selected_files = file_picker.selectedFiles()
            assert len(selected_files) == 1

            self.load_file(Path(selected_files[0]))

    def _on_save_clicked(self):
        file_picker = QFileDialog(self.ui, caption="Pick WireViz Output Directory")
        file_picker.setFileMode(QFileDialog.FileMode.Directory)

        if file_picker.exec_():
            selected_directories = file_picker.selectedFiles()
            assert len(selected_directories) == 1

            self.generate_wireviz_outputs(Path(selected_directories[0]))

    def _on_about_clicked(self):
        about_dlg = QMessageBox.about(self.ui, "About WireViz-GUI",
            f"WireViz-GUI (v {__version__})\n"
            "Developed by Thibaud, for you."
        )
