# WireViz GUI

## Development

### Updating the resource file
If you change the `.qrc` file, remember to regenerate it with

``` python
$ poetry run pyside6-rcc src/wireviz_gui/ui/ui-resources.qrc > src/wireviz_gui/ui/ui_resources.py
```